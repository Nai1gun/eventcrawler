package com.nailgun.newsstream.crawl;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
/**
 * @author nailgun
 * @since 23.02.13
 */
public class SessionDirCreator {

    public static File createSessionDir() {
        File sessionDir = FileUtils.getFile(FileUtils.getTempDirectoryPath(),
                ".eventstream", "session-" + System.currentTimeMillis());
        try {
            FileUtils.forceMkdir(sessionDir);
        } catch (IOException e) {
            throw new RuntimeException("Cannot create session dir!", e);
        }
        return sessionDir;
    }

}
