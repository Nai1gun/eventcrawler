package com.nailgun.newsstream.crawl;

import com.nailgun.newsstream.crawl.domain.Datasource;
import com.nailgun.newsstream.crawl.domain.PageTemplate;
import com.nailgun.newsstream.crawl.repository.DatasourceRepository;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;
import org.apache.nutch.crawl.Crawler;
import org.apache.nutch.indexer.elastic.ElasticIndexerJob;
import org.apache.nutch.util.NutchConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static org.apache.commons.lang3.StringUtils.*;

@Component
public class WebCrawler {

    private static Logger LOG = LoggerFactory.getLogger(WebCrawler.class);

    private static final String[] CRAWL_ARGS = split("-depth 3 -topN 5 ");

    private static final String[] INDEX_ARGS = split("elasticsearch -reindex");

    @Autowired
    private Properties nutchProps;

    @Autowired
    private DatasourceRepository datasourceRepository;

    //@Scheduled(cron = "0 30 3 ? * *")//Run at 3:30 every day
    public void nightly() {
        crawl();
    }

    @Scheduled(fixedRate = 1000 * 60 * 5)
    public void test() {
        crawl();
    }

    private void crawl() {
        List<String> urls = new ArrayList<String>();
        List<String> urlFilterRegexList = new ArrayList<String>();
        for(Datasource ds: datasourceRepository.findAll()) {
            urls.add(ds.getUrl());
            urlFilterRegexList.add("+^" + ds.getUrl() + "$");
            for (PageTemplate pt: ds.getPagetemplates()) {
                urlFilterRegexList.add(pt.getUrlRegex());
            }
        }
        File sessionDir = SessionDirCreator.createSessionDir();

        Job<String> seedJob = new SeedFileJob(urls, sessionDir);
        String urlsDir = seedJob.run();
        String[] args = ArrayUtils.add(CRAWL_ARGS, urlsDir);

        Job<String> filtersJob = new UrlFiltersRulesJob(urlFilterRegexList);
        String urlFilterRules = filtersJob.run();
        nutchProps.setProperty("urlfilter.regex.rules", urlFilterRules);
        Configuration nutchConf = NutchConfiguration.create(true, nutchProps);

        LOG.info("Run crawl job");
        try {
            ToolRunner.run(nutchConf, new Crawler(), args);
        } catch (Exception e) {
            LOG.error("Couldn't run crawl job! ", e);
            return;
        }

        LOG.info("Run index job");
        try {
            ToolRunner.run(nutchConf, new ElasticIndexerJob(), INDEX_ARGS);
        } catch (Exception e) {
            LOG.error("Couldn't run index job! ", e);
            return;
        }
    }

}