package com.nailgun.newsstream.crawl;

import java.util.Map;
import java.util.Properties;

public class NutchUtils {

    public static Properties nutchProps() {
        Map<String, String> envProps = System.getenv();
        String nutchHome = envProps.get("NUTCH_HOME");
        if (nutchHome == null) throw new RuntimeException(
                "Please set NUTCH_HOME environment variable to the path of Nutch distribution.");
        Properties props = new Properties();
        String sep = System.getProperty("file.separator");
        if (!nutchHome.endsWith(sep)) nutchHome = nutchHome + sep;
        props.setProperty("plugin.folders", nutchHome + "runtime/local/plugins");
        return props;
    }

}
