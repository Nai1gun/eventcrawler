package com.nailgun.newsstream.crawl;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * @author nailgun
 * @since 17.02.13
 */
public class SeedFileJob implements Job<String> {

    private static Logger LOG = LoggerFactory.getLogger(SeedFileJob.class);

    private List<String> urlList;

    private File urlsDir;

    public SeedFileJob(List<String> urlList, File urlsDir) {
        this.urlList = urlList;
        this.urlsDir = urlsDir;
    }

    /**
     * return seed dir name
     * @return
     */
    @Override
    public String run() {
        File seedFile = FileUtils.getFile(urlsDir, "seed.txt");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(seedFile);
            IOUtils.writeLines(urlList, "\n", fos);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(fos);
        }
        return urlsDir.getAbsolutePath();
    }
}
