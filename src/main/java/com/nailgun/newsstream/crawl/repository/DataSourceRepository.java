package com.nailgun.newsstream.crawl.repository;

import com.nailgun.newsstream.crawl.domain.Datasource;
import org.springframework.data.repository.CrudRepository;

/**
 * @author nailgun
 * @since 17.03.13
 */
public interface DatasourceRepository extends CrudRepository<Datasource, Integer> {
}
