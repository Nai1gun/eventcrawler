package com.nailgun.newsstream.crawl.domain;

import javax.persistence.*;

/**
 * @author nailgun
 * @since 17.03.13
 */
@javax.persistence.Table(name = "page_template", schema = "", catalog = "settings")
@Entity
public class PageTemplate {
    private int id;

    @javax.persistence.Column(name = "id")
    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    private String urlRegex;

    @javax.persistence.Column(name = "url_regex")
    @Basic
    public String getUrlRegex() {
        return urlRegex;
    }

    public void setUrlRegex(String urlRegex) {
        this.urlRegex = urlRegex;
    }

    private boolean event;

    @javax.persistence.Column(name = "event")
    @Basic
    public boolean isEvent() {
        return event;
    }

    public void setEvent(boolean event) {
        this.event = event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PageTemplate that = (PageTemplate) o;

        if (event != that.event) return false;
        if (id != that.id) return false;
        if (urlRegex != null ? !urlRegex.equals(that.urlRegex) : that.urlRegex != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (urlRegex != null ? urlRegex.hashCode() : 0);
        result = 31 * result + (event ? 1 : 0);
        return result;
    }

    private Datasource datasource;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "datasource_id")
    public Datasource getDatasource() {
        return datasource;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }
}
