package com.nailgun.newsstream.crawl;

/**
 *
 * @author  nailgun
 * @since 17.02.13
 */
public interface Job<T> {
    T run();
}
