# Event crawler


## Overview
This tiny project uses apache Nutch backed by HBase and ElasticSearch to crawl and index web sites.

It is planned to build events aggregation and recommendation system on it.

## Configuration:
 * Nutch-2.1
 * HBase-0.90.6
 * ElasticSearch-0.20.4

## Setup:
 1. Download and unpack Nutch
 2. Build Nutch with ant
 3. Set NUTCH_HOME
 4. Download and unpack HBase
 5. Set HBASE_HOME (optional)
 6. Download and unpack ElasticSearch
 7. Install ElasticSearch "head" plugin (optional)
 8. Start HBase
 9. Start ElasticSearch
 10. Run the application

## See also:
http://notebyxz.blogspot.ru/2012/10/building-java-application-with-nutch-20.html